# App Ko To (malupet-na-app) Read Carefully :one:
A Quasar Framework app

# Table of Contents
1. [Installation](#installation)
2. [Pre-Development / Install Dependencies](#pre-development)
3. [Firebase Setup](#firebase-setup)
4. [Development](#development)
5. [Deployment / Build](#deployment)


## Installation
### Download and open the file in your [Visual Studio Code](https://code.visualstudio.com/download) - (Easiest One) :smile:
Open new terminal under Terminal Menu Bar and Click "New Terminal" or use (CTRL + SHIFT + `) :keyboard: shortcut

### OR Clone the Repository (requires [git](https://git-scm.com/downloads)) - (Expert One) :sunglasses:
```
open any terminal
git clone https://gitlab.com/Masterxhen/batch6-quasar-app.git
cd batch6-quasar-app
```

# Pre-Development
### Install the dependencies (requires [Node JS](https://nodejs.org/en/) and [Yarn](https://yarnpkg.com/en/docs/install) Installed in your System)
```
yarn
```
# Firebase Setup
### Copy your configuration from the firebase console and change the firebase.js on src/boot/firebase.js
```
var firebaseConfig = {
  apiKey: "YOUR-API-KEY",
  authDomain: "YOUR-AUTH-DOMAIN",
  databaseURL: "YOUR-DATABASE-URL",
  projectId: "YOUR-PROJECT-ID",
  storageBucket: "YOUR-STORAGE-BUCKET",
  messagingSenderId: "YOUR-MSGSENDER-ID",
  appId: "YOUR-APP-ID"
};
```
# Development
### Start the app in development mode (hot-code reloading, error reporting, etc.)
```
quasar dev
```

# Deployment
### Build the app for production
```
quasar build
```

### Customize the configuration?
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
