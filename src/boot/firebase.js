// import something here
import firebase from "firebase/app"
import "firebase/firestore"
import { firestorePlugin } from "vuefire"

var firebaseConfig = {
  apiKey: "YOUR-API-KEY",
  authDomain: "YOUR-AUTH-DOMAIN",
  databaseURL: "YOUR-DATABASE-URL",
  projectId: "YOUR-PROJECT-ID",
  storageBucket: "YOUR-STORAGE-BUCKET",
  messagingSenderId: "YOUR-MSGSENDER-ID",
  appId: "YOUR-APP-ID"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// database
var db = firebase.firestore()
// "async" is optional
export default async ({ Vue }) => {
  // something to do
  // this.$db
  Vue.use(firestorePlugin)
  Vue.prototype.$db = db
}
